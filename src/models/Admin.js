const {model, Schema} = require('mongoose')
let adminSchema = new Schema({
    adminName:{
        type: String,
        unique: true,
        required: true,
    },
    password:{
        type: String,
        required: true,
    },
    role:{
        type: String,
        defaulft: "ADMIN"
    },
    rooms:[{
        ref: "Room",
        type: Schema.Types.ObjectId,
        default: [],
    }]
})
adminSchema.plugin(require('mongoose-autopopulate'));
module.exports = model('Admin', adminSchema)