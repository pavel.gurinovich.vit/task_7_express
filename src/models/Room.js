const {model, Schema} = require('mongoose')

let roomSchema = new Schema({
    number:{
        type: Number,
        required: true,
        unique: true,
    },
    users:[{
        ref: "User",
        type: Schema.Types.ObjectId,
        default: [],
    }],
    admin: {
        ref: "Admin",
        type: Schema.Types.ObjectId,
        autopopulate: true
    }
})
roomSchema.plugin(require('mongoose-autopopulate'));
module.exports = model('Room', roomSchema)