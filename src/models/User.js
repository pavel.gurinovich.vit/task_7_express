const {model, Schema} = require('mongoose')

let userSchema = new Schema({
    username:{
        type: String,
        unique: true,
        required: true,
    },
    password:{
        type: String,
        required: true,
    },
    role:{
        type: String,
        default: "USER"
    },
    rooms:[{
        ref: "Room",
        type: Schema.Types.ObjectId,
        default: [],
        autopopulate: true
    }]
})
userSchema.plugin(require('mongoose-autopopulate'));

module.exports = model('User', userSchema)