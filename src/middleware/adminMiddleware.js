const jwt = require("jsonwebtoken") 
const {secret} = require("../helpers/config")
module.exports = (role) => (req, res, next)=>{
        if(req.method === "OPTIONS"){
            next()
        }
        try{
            const token = req.headers.authorization.split(" ")[1]
            if(!token){
                return res.status(403).json({message: "Пользователь не авторизован"}) 
            }
            const decodedData = jwt.verify(token, secret).role
            let hasRole = false
            if(decodedData.includes(role)){
                hasRole = true
            }
            if(!hasRole){
                return res.statur(403).json({message: "У вас нет доступа"})
            }
            next()
        }catch(e){
            return res.status(403).json({message: "У вас нет доступа"})
        }
    }
