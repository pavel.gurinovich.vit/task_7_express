const mongoose = require("mongoose")
const {mongoUri} = require("../helpers/config")
const db = mongoose.connect(
    mongoUri,
    { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false
 },
    (err)=>{
        if(!err){
            console.log("MongoDB Connection Succeeded")
        }else{
            console.log("Error in DB connection: " + err)
        }
    })

module.exports = db