const jwt = require('jsonwebtoken')
const {secret} = require('./config')

module.exports = (id, role) =>{
    const payload = {
        id,
        role
    }
    return jwt.sign(payload, secret, {expiresIn: "24h"})
}

