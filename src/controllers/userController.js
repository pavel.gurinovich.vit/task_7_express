const User = require('../models/User')
const bCrypt = require('bcrypt')


const create_user = (req, res, next) =>{
    const {username, password} = req.body
    const hashPassword = bCrypt.hashSync(password, 7)
    User.create({username, password: hashPassword, role: "USER"}, (err, doc)=>{
        if(err) {
            return res.status(400).json({message: "Ошибка"})
        }
        res.json({message: doc})
    })
}

const read_user_info = (req, res, next) =>{
    let userID = req.params.id
    User.findById(userID, (err, doc)=>{
        if(err) {
            return res.status(400).json({message: "Ошибка"})
        }
        res.json({message: doc})
    })
}

const update_user_info = (req, res, next) =>{
    let userID = req.params.id
    User.findOneAndUpdate({_id: userID}, {username: req.body.username, password: req.body.password}, 
        {new: true}, (err, user)=>{
            if(err) {
                return res.status(400).json({message: "Ошибка"})
            }
            res.json({message: user})
    });
}

const delete_user = (req, res, next) =>{
    let userID = req.params.id
    User.findOneAndDelete({_id: userID}, (err, doc)=>{
        if(err) {
            return res.status(400).json({message: "Ошибка"})
        }
        res.json({message: "Пользователь удален"})
    });
}


module.exports = {
    create_user, 
    read_user_info, 
    update_user_info, 
    delete_user
}