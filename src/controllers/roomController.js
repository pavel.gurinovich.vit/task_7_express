const Room = require('../models/Room')
const jwt = require("jsonwebtoken") 
const {secret} = require("../helpers/config")
const Admin = require("../models/Admin")
const User = require("../models/User")

const create_room = async (req, res) => {
    try{
    const admin = await Admin.findOne({_id: req.user.id})
    const room = new Room({number: req.body.number})

    room.admin = req.user.id
    await room.save()

    admin.rooms.push(room._id)
    await admin.save()

    res.json({message: `Создана комната с номером ${room.number}`})
    }
    catch (err){
        return res.json({message: "Ошибка создания комнаты"})
    }
}

const add_user = async(req, res) => {
    try{
    const room = await Room.findOne({_id: req.params.id})
    room.users.push(req.user.id)
    await room.save()
    const user = await User.findOne({_id: req.user.id})
    user.rooms.push(req.params.id)
    await user.save()
    res.json({message:  "Пользователь добавлен"})
    }catch (err){
        return res.json({message: "Ошибка создания комнаты"})
    }
}

const get_rooms_list = async (req, res) => {
    try{
        let rooms
        if(req.user.role.toString() === "ADMIN"){
            rooms = await Room.find({admin: req.user.id})
        }else{ 
            rooms = await Room.find({users: req.user.id})
        }
        res.send(rooms)  
    }catch (err){
        return res.status(400).json({message: 'Ошибка получения данных'})
    }
}

const get_room_by_id = async (req, res) => {
    try{
        let room 
        const roomID = req.params.id
        if(req.user.role.toString() === "ADMIN"){
            room = await Room.findOne({_id: roomID})
        }else{ 
            room = await Room.findOne({_id: roomID, users: req.user.id})
        }
        if(!room){
            return res.status(400).json({message: 'У Вас нет доступа к этой комнате'})
        }
        res.send(room)
    }catch (err){
        return res.status(400).json({message: 'Ошибка получения данных'})
    }
}

const update_room = async (req, res) => {

}

const delete_room = async (req, res) => {
    try{
        let room = await Room.findOneAndDelete({_id: req.params.id, admin: req.user.id})
        delete_from_admin(req.user.id, req.params.id)
        delete_from_user(room.users, req.params.id)
        if(!room){
            return res.status(400).json({message: 'У Вас нет доступа к этой комнате'})
        }
        res.json({message: "Комната удалена"})
    }catch (err){
        return res.status(400).json({message: 'Ошибка получения данных'})
    }
}



const delete_from_admin = async (adminID, roomID) =>{
    let admin = await Admin.findOne({_id: adminID})
    for(let i=0;i<admin.rooms.length;i++){
        if(admin.rooms[i].toString() === roomID.toString()){
            admin.rooms.splice(i, 1)
            await admin.save()
            break
        }
    }
}

const delete_from_user = async (usersList, roomID)=>{
    for(let i=0;i<usersList.length;i++){
        const user = await User.findOne({_id: usersList[i].toString()})
        for(let j=0;j<user.rooms.length;j++){
            if(user.rooms[j].toString() === roomID.toString()){
                user.rooms.splice(j, 1)
                await user.save()
                break
            }
        }
    }
}


module.exports = {
    create_room,
    add_user,
    get_rooms_list,
    get_room_by_id,
    update_room,
    delete_room
}