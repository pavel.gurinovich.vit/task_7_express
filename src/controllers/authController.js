const bCrypt = require('bcrypt')
const User = require('../models/User')

const generateAccessToken = require('../helpers/generateAccessToken')

class authController {
    async registration(req, res){
        try{
            const {username, password} = req.body
            const candidate = await User.findOne({username})
            if(candidate){
                return res.status(400).json({message:"Пользователь с таким именем уже существует"})
            }
            const hashPassword = bCrypt.hashSync(password, 7)
            const user = new User({username, password: hashPassword})
            await user.save()

            return res.json({message: "Пользователь зарегестрирован"})
        }catch (e){
            res.status(400).json({message: "Registration error -> " + e})
        }
    }

    async login(req, res){
        try{
            const {username, password} = req.body
            const user = await User.findOne({username})
            if(!username){
                return res.status(400).json({message: `Пользователь ${username} не найден`})
            }
            const validPassword = bCrypt.compareSync(password, user.password)
            if(!validPassword){
                return res.status(400).json({message: `Введен неверный пароль`})
            }
            const token = generateAccessToken(user._id, user.role)
            
            return res.json({token: token})
        }catch(e){
            res.status(400).json({message: "Registration error -> " + e})
        }
    }
}

module.exports = new authController()