const Admin = require("../models/Admin")
const bCrypt = require('bcrypt')
const generateAccessToken = require('../helpers/generateAccessToken')

const create_admin = (req, res) => {
    const {adminName, password} = req.body
    const hashPassword = bCrypt.hashSync(password, 7)
    Admin.create({adminName, password: hashPassword, role: "ADMIN"}, 
    (err, doc)=>{
        if(err) {
            return res.status(400).json({message: err})
        }
        res.json({message:doc})
    })
}

const read_admin_info = (req, res) => {
    let adminID = req.params.id
    Admin.findById(adminID, (err, doc)=>{
        if(err) {
            return res.status(400).json({message: "Ошибка"})
        }
        res.json({message: doc})
    })
}

const update_admin_info = (req, res) =>{
    let adminID = req.params.id
    Admin.findOneAndUpdate({_id: adminID}, 
        {adminName: req.body.adminName, password: req.body.password}, {new: true}, 
        (err, user)=>{
            if(err) {
                return res.status(400).json({message: "Ошибка"})
            }
            res.json({message: user})
        });
}

const delete_admin = (req, res) => {
    let adminID = req.params.id
    Admin.findOneAndDelete({_id: adminID}, (err, doc)=>{
        if(err) {
            return res.status(400).json({message: "Ошибка"})
        }
        res.json({message: "Администратор удален"})
    });
}

const get_access_token = (req, res) => {
    let adminID = req.params.id
    res.json({token:generateAccessToken(adminID, "ADMIN")})
}


module.exports = {
    create_admin,
    read_admin_info,
    update_admin_info,
    delete_admin,
    get_access_token
}