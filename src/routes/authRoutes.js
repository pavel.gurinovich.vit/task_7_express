const authController = require("../controllers/authController")
const express = require("express")
const router = express.Router('express-validator')

router.post('/registration', authController.registration)

router.post('/login', authController.login)


module.exports = router 