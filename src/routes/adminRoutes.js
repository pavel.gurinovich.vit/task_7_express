const express = require("express")
const adminController = require("../controllers/adminController")
const router = express.Router()

router.post('/create', adminController.create_admin) 

router.get('/:id', adminController.read_admin_info)
router.post('/token/:id', adminController.get_access_token)
router.put('/:id', adminController.update_admin_info)

router.delete('/:id', adminController.delete_admin)

module.exports = router