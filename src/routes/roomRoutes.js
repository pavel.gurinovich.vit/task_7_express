const express = require("express")
const router = express.Router()
const roomController = require("../controllers/roomController")
const adminMiddleware = require("../middleware/adminMiddleware")
const authMiddleware = require("../middleware/authMiddleware")

router.post("/create", [authMiddleware, adminMiddleware("ADMIN")], roomController.create_room)
router.get("/get_list", authMiddleware, roomController.get_rooms_list)
router.get("/by_id/:id",authMiddleware, roomController.get_room_by_id)
router.get("/add/:id", adminMiddleware("USER"), roomController.add_user)
router.put("/:id", adminMiddleware("ADMIN"), roomController.update_room)
router.delete("/:id", [authMiddleware, adminMiddleware("ADMIN")], roomController.delete_room)




module.exports = router