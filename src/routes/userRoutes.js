const express = require("express")
const router = express.Router()
const userController =  require("../controllers/userController")

router.post('/create', userController.create_user) 

router.get('/:id', userController.read_user_info)

router.put('/:id', userController.update_user_info)

router.delete('/:id', userController.delete_user)

module.exports = router


