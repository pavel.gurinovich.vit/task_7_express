const express = require("express")
const userRouter = require("./src/routes/userRoutes")
const adminRouter = require("./src/routes/adminRoutes")
const authRouter = require("./src/routes/authRoutes")
const roomRouter = require("./src/routes/roomRoutes")

const parser = express.json()
const bodyParser = require("body-parser")
const urlencodedParser = bodyParser.urlencoded({extende: true})

const PORT = 3000
const app = express()

require("./src/db/db")

app.use(parser)
app.use(urlencodedParser)

app.use("/user", userRouter);
app.use("/admin", adminRouter);
app.use("/auth", authRouter)
app.use("/room", roomRouter)


app.listen(PORT, ()=>{
    console.log('Serser is running on port: ' + PORT)
})